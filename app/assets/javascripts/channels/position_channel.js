document.addEventListener('turbolinks:load', function() {
  var userId = document.querySelector('body').dataset.userId;
  App.cable.subscriptions.create({ channel: "PositionChannel", user_id: userId }, {
    received(data) {
      console.log(data)
      var lastPosition = marker._latlng;
      marker.remove();
      var polyline = L.polyline(
        [
          [lastPosition.lat, lastPosition.lng],
          [data.latitude, data.longitude],
        ],
        { color: 'red' }
      ).addTo(map);
      marker = L.marker([data.latitude, data.longitude]).addTo(map);
    }
  });
});
