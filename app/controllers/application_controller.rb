class ApplicationController < ActionController::Base
  skip_before_action :verify_authenticity_token, only: [:ulogger]

  def healthz
    head :ok
  end

  def ulogger
    if request.raw_post =~ /action=auth/ # pass=demo&action=auth&user=demo
      user = User.find_by(name: params[:user])
      if user&.valid_password?(params[:pass])
        session = SecureRandom.hex
        $redis.hset 'sessions', session, user.id
        cookies['ulogger'] = session
        render json: { error: false }
      else
        render status: :unauthorized,
               json: { error: true, message: 'Unauthorized' }
      end
    else
      user = User.find_by(id: $redis.hget('sessions', cookies['ulogger']))
      unless user
        return render status: :unauthorized,
                      json: { error: true, message: 'Unauthorized' }
      end
      if request.raw_post =~ /action=addtrack/ # action=addtrack&track=Auto_2019.09.29_16.01.53
        track = Track.find_or_create_by name: params[:track], user: user
        render json: { error: false, trackid: track.id }
      elsif request.raw_post =~ /action=addpos/ # altitude=111.0&provider=gps&trackid=1&accuracy=31.0&action=addpos&lon=13.37744557&time=1569765732&lat=52.48467407&speed=0.0
        position = Position.find_or_create_by(
          altitude: params[:altitude],
          provider: params[:provider],
          track_id: params[:trackid],
          accuracy: params[:accuracy],
          longitude: params[:lon],
          tracked_at: Time.at(params[:time].to_i).to_datetime,
          latitude: params[:lat],
          speed: params[:speed],
          bearing: params[:bearing],
          battery_status: params[:battery_status],
          battery_level: params[:battery_level]
        )
        render json: { error: false }
      elsif request.raw_post =~ /action=addacc/ # {"trackid"=>"14", "x"=>"1.72801506519318", "y"=>"6.83604431152344", "z"=>"9.47864437103272", "time"=>"1591342439"}
        accelerations = []
        if params[:trackid] && params[:x] && params[:y] && params[:z] && params[:time]
          accelerations = [params]
        elsif params[:accelerations]
          accelerations = params[:accelerations].split(';').map do |acceleration|
            trackid, x, y, z, time = acceleration.split(',')
            {
              trackid: trackid,
              x: x,
              y: y,
              z: z,
              time: time
            }
          end
        end
        accelerations.each do |acceleration|
          Acceleration.create(
            track_id: acceleration[:trackid],
            x: acceleration[:x],
            y: acceleration[:y],
            z: acceleration[:z],
            tracked_at: Time.at(acceleration[:time].to_i).to_datetime
          )
        end
        render json: { error: false }
      end
    end
  end

  private

  def authenticate
    authenticate_or_request_with_http_basic do |username, password|
      @user = User.find_by name: username
      if @user&.valid_password?(password)
        sign_in :user, @user
      end
    end
    warden.custom_failure! if performed?
  end
end
