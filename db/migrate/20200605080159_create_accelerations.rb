class CreateAccelerations < ActiveRecord::Migration[6.0]
  def change
    create_table :accelerations do |t|
      t.decimal :x
      t.decimal :y
      t.decimal :z
      t.datetime :tracked_at

      t.timestamps
    end
  end
end
