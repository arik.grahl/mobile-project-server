class AddUserToTracks < ActiveRecord::Migration[5.2]
  def change
    add_reference :tracks, :user, index: true, foreign_key: true
  end
end