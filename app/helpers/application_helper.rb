module ApplicationHelper
  def user_color_class(user)
    colors = %w[red pink purple deep-purple indigo blue light-blue cyan teal green light-green lime yellow amber orange deep-orange brown]
    colors[user.id % colors.count]
  end
end
