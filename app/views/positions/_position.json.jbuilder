json.extract! position, :id, :altitude, :provider, :accuracy, :longitude, :latitude, :tracked_at, :speed, :created_at, :updated_at
json.url position_url(position, format: :json)
