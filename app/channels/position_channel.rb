class PositionChannel < ApplicationCable::Channel
  def subscribed
    stream_from "position_#{params[:user_id]}"
  end
end
