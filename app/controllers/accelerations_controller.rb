class AccelerationsController < ApplicationController
  before_action :set_acceleration, only: [:show, :edit, :update, :destroy]

  # GET /accelerations
  # GET /accelerations.json
  def index
    @accelerations = Acceleration.all
  end

  # GET /accelerations/1
  # GET /accelerations/1.json
  def show
  end

  # GET /accelerations/new
  def new
    @acceleration = Acceleration.new
  end

  # GET /accelerations/1/edit
  def edit
  end

  # POST /accelerations
  # POST /accelerations.json
  def create
    @acceleration = Acceleration.new(acceleration_params)

    respond_to do |format|
      if @acceleration.save
        format.html { redirect_to @acceleration, notice: 'Acceleration was successfully created.' }
        format.json { render :show, status: :created, location: @acceleration }
      else
        format.html { render :new }
        format.json { render json: @acceleration.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /accelerations/1
  # PATCH/PUT /accelerations/1.json
  def update
    respond_to do |format|
      if @acceleration.update(acceleration_params)
        format.html { redirect_to @acceleration, notice: 'Acceleration was successfully updated.' }
        format.json { render :show, status: :ok, location: @acceleration }
      else
        format.html { render :edit }
        format.json { render json: @acceleration.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /accelerations/1
  # DELETE /accelerations/1.json
  def destroy
    @acceleration.destroy
    respond_to do |format|
      format.html { redirect_to accelerations_url, notice: 'Acceleration was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_acceleration
      @acceleration = Acceleration.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def acceleration_params
      params.require(:acceleration).permit(:x, :y, :z, :tracked_at)
    end
end
