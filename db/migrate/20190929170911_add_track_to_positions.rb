class AddTrackToPositions < ActiveRecord::Migration[5.2]
  def change
    add_reference :positions, :track, index: true, foreign_key: true
  end
end