class PositionsController < ApplicationController
  before_action :authenticate
  before_action :set_users, only: [:index]
  before_action :set_users_tracks, only: [:index]
  before_action :set_all_users, only: [:index]
  before_action :set_first_position, only: [:index]
  before_action :set_last_position, only: [:index]
  before_action :redirect_to_canoncial_url, only: [:index]
  before_action :set_since, only: [:index]
  before_action :set_until, only: [:index]
  before_action :set_positions, only: [:index]
  before_action :set_position, only: [:show, :edit, :update, :destroy]

  # GET /positions
  # GET /positions.json
  def index
    respond_to do |format|
      format.html {}
      format.json do
        render json: @positions.transform_keys { |user| user.name },
               except: [:created_at, :updated_at],
               include: { accelerations: { only: [:x, :y, :z, :tracked_at] } }
      end
      format.csv do
        require 'csv'
        filename = "positions-#{Time.now.strftime('%Y-%m-%d-%H-%M-%S')}.csv"
        response.headers['Content-Type'] = 'text/csv'
        response.headers['Content-Disposition'] = "attachment; filename=#{filename}"
        csv_string = CSV.generate do |csv|
          attributes = {
           user: %i[name],
           track: %i[id],
           position: %i[id altitude provider accuracy longitude latitude tracked_at speed battery_status battery_level bearing],
           acceleration: %i[x y z tracked_at]
          }
          csv << attributes.map do |model, attributes|
            attributes.map { |attribute| "#{model}_#{attribute}" }
          end.flatten
          select = attributes.map do |model, attributes|
            attributes.map do |attribute|
              "#{model}s.#{attribute} AS #{model}_#{attribute}"
            end.join(', ')
          end.join(', ')
          query = <<-SQL
            SELECT #{select}
            FROM accelerations
            FULL OUTER JOIN positions
            ON accelerations.track_id = positions.track_id
            INNER JOIN tracks
            ON accelerations.track_id = tracks.id
            INNER JOIN users
            ON tracks.user_id = users.id
            WHERE
              (
                ('#{@since.strftime('%Y-%m-%d %H:%M:%S')}' <= accelerations.tracked_at AND accelerations.tracked_at <= '#{@until.strftime('%Y-%m-%d %H:%M:%S')}') OR
                ('#{@since.strftime('%Y-%m-%d %H:%M:%S')}' <= positions.tracked_at AND positions.tracked_at <= '#{@until.strftime('%Y-%m-%d %H:%M:%S')}')
              ) AND accelerations.tracked_at = positions.tracked_at AND users.id in (#{params[:users].join(', ')})
          SQL
          ActiveRecord::Base.connection.execute(query).each do |row|
            csv << row.values
          end
        end
        render plain: csv_string
      end
    end
  end

  # GET /positions/1
  # GET /positions/1.json
  def show
  end

  # GET /positions/new
  def new
    @position = Position.new
  end

  # GET /positions/1/edit
  def edit
  end

  # POST /positions
  # POST /positions.json
  def create
    @position = Position.new(position_params)

    respond_to do |format|
      if @position.save
        format.html { redirect_to @position, notice: 'Position was successfully created.' }
        format.json { render :show, status: :created, location: @position }
      else
        format.html { render :new }
        format.json { render json: @position.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /positions/1
  # PATCH/PUT /positions/1.json
  def update
    respond_to do |format|
      if @position.update(position_params)
        format.html { redirect_to @position, notice: 'Position was successfully updated.' }
        format.json { render :show, status: :ok, location: @position }
      else
        format.html { render :edit }
        format.json { render json: @position.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /positions/1
  # DELETE /positions/1.json
  def destroy
    @position.destroy
    respond_to do |format|
      format.html { redirect_to positions_url, notice: 'Position was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_position
      @position = Position.find(params[:id])
    end

    def set_users
      # @users = User.all unless params[:users]&.any?
      # @users ||= User.where(id: params[:users])
      @users = [@user]
    end

    def set_users_tracks
      @users_tracks = @users.map(&:tracks).flatten
    end

    def set_all_users
      # @all_users = User.all
      @all_users = [@user]
    end

    def set_since
      @since = DateTime.parse(params[:since]) if params[:since]
      @since ||= @first_position.tracked_at
    end

    def set_until
      @until = DateTime.parse(params[:until]) if params[:until]
      @until ||= @last_position.tracked_at
    end

    def set_positions
      @positions = {}
      @users.each do |user|
        user_positions = Position.where(track: user.tracks)
                                 .where(tracked_at: @since..@until)
                                 .order(tracked_at: :asc)
        @positions[user] = user_positions if user_positions.any?
      end
    end

    def set_first_position
      @first_position = Position.where(track: @users_tracks)
                                .order(tracked_at: :asc)
                                .first
    end

    def set_last_position
      @last_position = Position.where(track: @users_tracks)
                               .order(tracked_at: :asc)
                               .last
    end

    def redirect_to_canoncial_url
      unless params[:since] && params[:until] && params[:users]
        params[:since] ||= 1.day.ago.at_beginning_of_day
        params[:until] ||= Time.now.at_end_of_day
        params[:users] ||= @all_users.map(&:id)
        redirect_to positions_path(since: params[:since], until: params[:until], users: params[:users])
      end
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def position_params
      params.require(:position).permit(:altitude, :provider, :accuracy, :longitude, :latitude, :tracked_at, :speed, :battery_status, :battery_level)
    end
end
