#!/usr/bin/env ruby
# bin/add-n-to-one-reference.rb

require 'rails'

begin
  n_entity = ARGV[0]
  n_entities = n_entity.pluralize
  one_entity= ARGV[1]
  raise unless n_entity && one_entity
rescue
  puts "usage: ./#{$0} N_ENTITY ONE_ENTITY"
  exit 1
end

Time.zone = 'Europe/Berlin'
timestamp = Time.zone.now.strftime('%Y%m%d%H%M%S')

File.open("db/migrate/#{timestamp}_add_#{one_entity}_to_#{n_entities}.rb", 'w') do |file|
  file.write("class Add#{one_entity.camelize}To#{n_entities.camelize} < ActiveRecord::Migration[5.2]\n")
  file.write("  def change\n")
  file.write("    add_reference :#{n_entities}, :#{one_entity}, index: true, foreign_key: true\n")
  file.write("  end\n")
  file.write("end")
end
