class Track < ApplicationRecord
  belongs_to :user
  has_many :positions
  has_many :accelerations
end
