class AddBearingToPositions < ActiveRecord::Migration[6.0]
  def change
    add_column :positions, :bearing, :decimal
  end
end
