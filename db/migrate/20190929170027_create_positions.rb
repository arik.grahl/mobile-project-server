class CreatePositions < ActiveRecord::Migration[6.0]
  def change
    create_table :positions do |t|
      t.decimal :altitude
      t.string :provider
      t.decimal :accuracy
      t.decimal :longitude
      t.decimal :latitude
      t.datetime :tracked_at
      t.decimal :speed

      t.timestamps
    end
  end
end
