class Position < ApplicationRecord
  belongs_to :track
  # after_create :broadcast

  def accelerations
    Acceleration.where(track: self.track, tracked_at: self.tracked_at)
  end

  private

  # def broadcast
  #   ActionCable.server.broadcast "position_#{track.user}",
  #                                latitude: latitude,
  #                                longitude: longitude
  # end
end
