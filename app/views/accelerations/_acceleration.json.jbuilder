json.extract! acceleration, :id, :x, :y, :z, :tracked_at, :created_at, :updated_at
json.url acceleration_url(acceleration, format: :json)
