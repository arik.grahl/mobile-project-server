//= link apple-touch-icon.png
//= link favicon-32x32.png
//= link favicon-16x16.png
//= link manifest.json
//= link safari-pinned-tab.svg
//= link favicon.ico
//= link browserconfig.xml
//
//= link_tree ../images
//= link_directory ../javascripts .js
//= link_directory ../stylesheets .css
