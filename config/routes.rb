Rails.application.routes.draw do
  mount ActionCable.server => '/cable'
  get '/' => redirect('/positions')
  get '/healthz' => 'application#healthz'
  post '/client/index.php' => 'application#ulogger'
  resources :positions, only: [:index]
end
