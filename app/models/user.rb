class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable, :omniauthable, :registerable, :recoverable, :rememberable, :validatable
  devise :database_authenticatable
  validates :name, presence: true
  has_many :tracks
  has_many :positions, through: :tracks
end
