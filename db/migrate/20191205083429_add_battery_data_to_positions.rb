class AddBatteryDataToPositions < ActiveRecord::Migration[6.0]
  def change
    add_column :positions, :battery_status, :string
    add_column :positions, :battery_level, :float
  end
end
