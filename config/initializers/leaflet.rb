Leaflet.tile_layer = ENV.fetch('TILE_SERVER_URL', 'https://tile.openstreetmap.org/{z}/{x}/{y}.png')
Leaflet.attribution = '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
Leaflet.max_zoom = 18
