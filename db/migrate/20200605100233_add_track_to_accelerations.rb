class AddTrackToAccelerations < ActiveRecord::Migration[5.2]
  def change
    add_reference :accelerations, :track, index: true, foreign_key: true
  end
end